FROM python:3.7

WORKDIR /usr/local/app

RUN apt update -y && \
  apt update -y

COPY requirements.txt ./
RUN pip3 install -r requirements.txt
COPY gitlab_assessment/ /usr/local/app
ENTRYPOINT [ "python3" ]
CMD ["/usr/local/app/gitlab_assessment.py"]