"""Main module."""
import yaml
import os
import pandas as pd


def main():
    completed_array = {"group_name": [],
                       "group_managers": [], "group_members": []}
    manager_positions = ["backend_engineering_manager",
                         "frontend_engineering_manager", ]
    positions = ["pm", "pmm", "cm", "support", "sets", "pdm",
                 "ux", "uxr", "tech_writer", "tw_backup", "appsec_engineer"]
    groups = load_file('gl_dept_group.yml')
    members = load_file('data_stages.yml')
    eng_dept_groups = get_eng_department_groups(groups)

    for index, row in eng_dept_groups.iterrows():
        managers = get_managers(
            members, row['main_group'], row['secondary_group'], manager_positions)
        group_members = get_members(
            members, row['main_group'], row['secondary_group'], positions)
        completed_array['group_name'].append(row['full_group_name'])
        completed_array['group_managers'].append(managers)
        completed_array['group_members'].append(group_members)
    dataframe = pd.DataFrame(completed_array)
    final_array = []
    for index, row, in dataframe.iterrows():
        final_dictionary = {'group_name': None,
                            'group_managers': None, 'group_members': None}
        final_dictionary['group_name'] = row['group_name']
        final_dictionary['group_managers'] = row['group_managers']
        final_dictionary['group_members'] = row['group_members']
        final_array.append(final_dictionary)
    for row in final_array:
        print(row)


def load_file(file_path):
    """Function used to load files into python script

    Parameters
    ----------
    file_path : str
        File name, will have to be in the same directory as the script itself.

    Returns
    -------
    dict
        The contents of the file in a dict format.
    """
    try:
        file_contents = yaml.safe_load(open(os.path.dirname(
            os.path.realpath(__file__)) + '/' + file_path))
        return file_contents
    except Exception as e:
        print(e)


def get_eng_department_groups(department_groups):
    """Function used to get and parse each department group into a
    full_group_name, main_group, and secondary_group.
    These are later used in order to find each member and manager of each
    department as well as producting the final array.

    Parameters
    ----------
    department_groups : dict
        The department groups that are imported from the load_file function.

    Returns
    -------
    DataFrame
        Easy to use and parse dataframe containing the full_group_name,
        main_group, and secondary_group.
    """
    groups = {'full_group_name': [], 'main_group': [], 'secondary_group': []}
    for group in department_groups['gl_dept_group']:
        if group.startswith('eng-dev-'):
            groups['full_group_name'].append(group)
            regex_group = group.replace('eng-dev-', '')
            split_group = regex_group.split('-', 1)
            main_group = split_group[0]
            secondary_group = split_group[1]
            secondary_group = secondary_group.replace('-', '_')
            secondary_group = secondary_group.replace('mgmt', 'management')
            secondary_group = secondary_group.replace(
                'ci', 'continuous_integration')
            groups['main_group'].append(main_group)
            groups['secondary_group'].append(secondary_group)
    groups_pd = pd.DataFrame(groups)
    return groups_pd


def get_members(group, main_group, secondary_group, jobs):
    """Function that will return all of the group members for each of group
    passed into the function.

    Parameters
    ----------
    group : str
        Group name for finding each of the members of the team.
    main_group : str
        Main group name for finding each of the members of the team.
    secondary_group : str
        Secondary group name for finding each of the members of the team.
    jobs : list
        List of job titles that does not contain the manager titles.

    Returns
    -------
    list
        List of all of the members of each secondary group of each team.
    """
    members = []
    for item, doc in group.items():
        for job in jobs:
            try:

                job_members = doc[main_group]['groups'][secondary_group][job]
                if isinstance(job_members, list):
                    for row in job_members:
                        if row != 'TBD':
                            members.append(row)
                else:
                    if (job_members and job_members != 'TBD' and job_members
                            != "TBD (SM)"):
                        members.append(job_members)
            except:
                pass
    return members


def get_managers(group, main_group, secondary_group, jobs):
    """Function used to gather the managers of each group.

    Parameters
    ----------
    group : str
        Group name for finding each of the members of the team.
    main_group : str
        Main group name for finding each of the members of the team.
    secondary_group : str
        Secondary group name for finding each of the members of the team.
    jobs : list
        List of job titles for manager positions of each team.

    Returns
    -------
    list
        The group managers of each secondary group of the team.
    """
    managers = []
    for item, doc in group.items():
        for job in jobs:
            try:
                job_managers = doc[main_group]['groups'][secondary_group][job]
                if isinstance(job_managers, list):
                    for each in job_managers:
                        managers.append(each)
                else:
                    managers.append(job_managers)
            except:
                pass
    return managers


if __name__ == '__main__':
    main()
