#!/usr/bin/env python

"""Tests for `gitlab_assesment` package."""

import pytest

from gitlab_assesment import gitlab_assesment


@pytest.mark.parametrize('file_path, expected', [('gl_dept_group.csv', not None), ('not_gl_dept_group.csv', Exception), ('data_stages.yml', not None), ('data_stages.yml', Exception)])
def test_load_file(file_path, expected):
    new_file = gitlab_assesment.load_file(file_path)
    assert expected


@pytest.mark.parametrize('groups, expected', [({'gl_dept_group': ['eng-dev-manage-compliance', 'eng-dev-manage-access', 'ga-recruiting', 'eng-dev-plan-project-mgmt']}, 'eng-dev-manage-compliance')])
def test_get_eng_department_groups(groups, expected):
    eng_department_groups = gitlab_assesment.get_eng_department_groups(groups)
    assert eng_department_groups['full_group_name'][0] == expected
    assert eng_department_groups['secondary_group'][2] == 'project_management'

