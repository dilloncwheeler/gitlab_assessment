# Project Title

Gitlab Assesment

## Description

This is a simple proof of concept script completed for a GitLab assessment. It is used to parse the information provided by GitLab to determine all of the managers and members of each group in their dev department.

## Getting Started

### Dependencies

Docker

### Installing
```
git clone git@gitlab.com:dilloncwheeler/gitlab_assessment.git
```
```
docker build gitlab_assessment -t gitlab_assessment
```

### Executing program

```
docker run gitlab_assessment
```

